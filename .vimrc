" Automatic installation plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" vim syntax plugin for Ansible 2.x
Plug 'pearofducks/ansible-vim'

" file system explorer
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Elegant vim theme with bright colors
Plug 'connorholyday/vim-snazzy'

" A light and configurable statusline/tabline
Plug 'itchyny/lightline.vim'

" Initialize plugin system
call plug#end()

set nocompatible "vi is gone, long live vim

set background=dark "dark background readability
colorscheme snazzy

" Use :help 'option' to see the documentation for the given option.
" To reload vim, use :so %

set autoindent

" Make backspace behave in a sane manner.
set backspace=indent,eol,start

" Switch syntax highlighting on
syntax on

" Enable file type detection and do language-dependent indenting.
filetype plugin indent on

" Toggling paste mode
set pastetoggle=<F2>

" turn off auto-insert of comments
augroup auto_comment
    au!
    au FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
augroup END     

" use clipboard as default register
set clipboard=unnamedplus

" enable statusbar
set laststatus=2
set ruler
let g:lightline = {
\ 'colorscheme': 'snazzy',
\ }

" automatic line numerb toggling
:set number relativenumber
" toggle line numbers
noremap <F3> :set invnumber invrelativenumber<CR>
inoremap <F3> <C-O>:set invnumber invrelativenumber<CR>

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" use UTF-8
set encoding=utf-8

" remap nerdtree toggle
nmap <F6> :NERDTreeToggle<CR>
