# dotfiles

## TODO
### Insync
- [ ] Backup and Restore .config folder
- [ ] do not start application after installation
- [ ] Only copy if not done yet

## Installation

Install git-crypt and git
```shell
sudo apt update && sudo apt install --assume-yes git-crypt git
git-crypt unlock <gitcrypt-key>
```

Clone this repository
```shell
git clone git@gitlab.com:doxic/dotfiles.git
```

Run scripts
```shell
./install-vsc.sh
./apt-install.sh
./install-zsh.sh
./install-docker.sh
./install-hypervisor.sh
./setup-python.sh
./symlink.sh

# create a new profile first!
./setup-terminal.sh

./install-pia.sh
./install-insync.sh
./install-remmina.sh
./install-snx.sh
./install-x2go.sh
./install-yubikey.sh
```

## Restore sensitive files

Restore GPG from backup
```shell
cp -Rf /media/${USER}/Backup/.gnupg/{private-keys-v1.d,pubring.kbx,trustdb.gpg} ${HOME}/.gnupg/
```

Backup GPG
- [How to migrate or export all GnuPG (gpg) public and private keys from one user to another - Red Hat Customer Portal](https://access.redhat.com/solutions/2115511)
```
gpg -a --export >/media/${USER}/Backup/${HOSTNAME}-${USER}-pubkeys.asc
gpg -a --export-secret-keys >/media/${USER}/Backup/${HOSTNAME}-${USER}-secret-keys.asc
gpg --export-ownertrust >>/media/${USER}/Backup/ownertrust.txt
```

Import GPG
```
gpg --import /media/${USER}/Backup/*-pubkeys.asc
gpg --import /media/${USER}/Backup/*-secret-keys.asc
gpg --import-ownertrust /media/${USER}/Backup/ownertrust.txt
gpg -K
gpg -k
```