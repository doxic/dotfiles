#!/bin/bash

which insync &> /dev/null
if [ $? -ne 0 ]; then
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
  echo "deb http://apt.insync.io/mint $(lsb_release --short --codename) non-free contrib" | sudo tee /etc/apt/sources.list.d/insync.list
  sudo apt update
  sudo apt install -y insync
fi
