#!/bin/bash

# Fixing the "Open/Save File" dialog
# https://www.reddit.com/r/matedesktop/comments/gi3hc9/fixing_the_opensave_file_dialog_search_vs_change/
dconf read /org/gtk/settings/file-chooser/location-mode | grep filename &> /dev/null
if [ $? -ne 0 ]; then
    dconf write /org/gtk/settings/file-chooser/location-mode \"filename-entry\"
fi
