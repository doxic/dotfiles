#!/bin/bash

git clone https://github.com/tobark/hyper-snazzy-gnome-terminal.git "$HOME/.zsh/hyper-snazzy-gnome-terminal"
chmod +x $HOME/.zsh/hyper-snazzy-gnome-terminal/hyper-snazzy.sh && $HOME/.zsh/hyper-snazzy-gnome-terminal/hyper-snazzy.sh

git clone https://github.com/arcticicestudio/nord-gnome-terminal.git "$HOME/.zsh/nord-gnome-terminal"
pushd $HOME/.zsh/nord-gnome-terminal/src
./nord.sh
popd

mkdir -p ~/.fonts
cp -r fonts/* ~/.fonts
fc-cache -vf ~/.fonts