# Path to your oh-my-zsh installation.
  export ZSH="/home/dominic/.oh-my-zsh"

# Set name of the theme to load
ZSH_THEME=""

# Uncomment the following line to automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="dd.mm.yyyy"

# enable ? in prompt
unsetopt nomatch

plugins=(
  git
  zsh-syntax-highlighting
  zsh-autosuggestions
  ssh-agent
  common-aliases
  virtualenv
)

#zstyle :omz:plugins:ssh-agent agent-forwarding on
#zstyle :omz:plugins:ssh-agent identities id_ed25519

source $ZSH/oh-my-zsh.sh

# User configuration

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
export EDITOR='vim'

# DOCKER
if [[ "$(< /proc/sys/kernel/osrelease)" == *Microsoft ]]; then 
  export DOCKER_CERT_PATH=/c/Users/dominic.ruettimann/.docker/machine/certs/
  export DOCKER_HOST=tcp://192.168.99.100:2376
  export DOCKER_TLS_VERIFY=1
  export COMPOSE_CONVERT_WINDOWS_PATHS=1
fi

# SSH
if [ ! -d "$HOME/.ssh/sockets" ] ; then
    mkdir -p "$HOME/.ssh/sockets"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# gpg-agent
#export GPG_TTY="$(tty)"
#export SSH_AUTH_SOCK="/run/user/$UID/gnupg/S.gpg-agent.ssh"
#gpg-connect-agent updatestartuptty /bye
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

# hook direnv into shell
eval "$(direnv hook zsh)"

# aliases
alias "terraform apply"="terraform apply --auto-approve"

# pure ZSH prompt
fpath+=$HOME/.zsh/pure
autoload -U promptinit; promptinit
prompt pure
