#!/bin/bash

which remmina &> /dev/null
if [ $? -ne 0 ]; then
    sudo apt-add-repository -y ppa:remmina-ppa-team/remmina-next
    sudo apt update
    sudo apt install -y remmina remmina-plugin-rdp remmina-plugin-secret
fi