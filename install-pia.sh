#!/bin/bash

which piactl &> /dev/null
if [ $? -ne 0 ]; then
    curl -fsSL -o pia-linux.run https://installers.privateinternetaccess.com/download/pia-linux-2.1-04977.run
    sh pia-linux.run
    rm pia-linux.run
fi
