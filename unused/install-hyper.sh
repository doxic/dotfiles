#!/bin/bash

which hyper &> /dev/null
if [ $? -ne 0 ]; then
  wget https://releases.hyper.is/download/deb
  sudo dpkg -i deb
  sudo apt install -fy
  rm deb
fi

# Nerd Font
fc-list | grep -i Hasklig &> /dev/null

if [ $? -ne 0 ]; then
  wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Hasklig.zip
  unzip Hasklig.zip -d ~/.fonts
  fc-cache -fv
  rm Hasklig.zip
fi
