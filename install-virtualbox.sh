#!/bin/bash

# Virtualbox
which vboxmanage &> /dev/null
if [ $? -ne 0 ]; then
    source /etc/os-release
    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
    echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $UBUNTU_CODENAME contrib" | sudo tee /etc/apt/sources.list.d/insync.list
    sudo apt update
    sudo apt install -y virtualbox-6.1 libarchive-tools virtualbox-ext-pack
    # sudo DEBIAN_FRONTEND=noninteractive apt install -y virtualbox virtualbox-ext-pack
fi

# Any system user who is going to use USB devices from Oracle VM VirtualBox guests must be a member group vboxusers
groups $(logname) | grep -qne "\bvboxusers\b" || sudo gpasswd --add $(logname) vboxusers