#!/bin/bash

which cfssl &> /dev/null
if [ $? -ne 0 ]; then
    curl -L -o cfssl https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
    curl -L -o cfssljson https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
    curl -L -o cfssl-certinfo https://pkg.cfssl.org/R1.2/cfssl-certinfo_linux-amd64
    chmod +x cfssl*
    sudo mv cfssl* /usr/local/bin/
fi