#!/bin/bash

which snx &> /dev/null
if [ $? -ne 0 ]; then
    wget -O snx_install_linux30.sh https://www.dropbox.com/s/moxkn9hbsjocurs/snx_install_linux30.sh?dl=0
    
    sudo apt-get install -y libpam0g:i386 libx11-6:i386 libstdc++5:i386
    
    chmod +x snx_install_linux30.sh
    sudo ./snx_install_linux30.sh
    rm snx_install_linux30.sh
fi