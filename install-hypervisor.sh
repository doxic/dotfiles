#!/bin/bash

# Virtualbox
which vboxmanage &> /dev/null
if [ $? -ne 0 ]; then
    sudo apt update
    sudo DEBIAN_FRONTEND=noninteractive apt install -y virtualbox virtualbox-ext-pack
fi

# Vagrant
VAGRANTVERSION=2.2.14

which vagrant &> /dev/null
if [ $? -ne 0 ]; then
    curl -o vagrant.zip "https://releases.hashicorp.com/vagrant/"$VAGRANTVERSION"/vagrant_"$VAGRANTVERSION"_linux_amd64.zip"
    unzip vagrant.zip
    sudo mv vagrant /usr/local/bin/
    rm -f vagrant.zip
fi