#!/bin/bash

which x2goclient &> /dev/null
if [ $? -ne 0 ]; then
    sudo apt-add-repository -y ppa:x2go/stable
    sudo apt update
    sudo apt install -y x2goclient
fi